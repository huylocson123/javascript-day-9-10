function findIndex(listEmploy, acc) {
    return viTri = listEmploy.findIndex(function(item) {
        return item.account == acc;
    });

}

function getEmployees() {
    var account = document.getElementById("tknv").value;
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var dayBegin = document.getElementById("datepicker").value;
    var getPos = document.getElementById("chucvu");
    var position = getPos.options[getPos.selectedIndex].value;
    var workTime = document.getElementById("gioLam").value;
    var salary = document.getElementById("luongCB").value;

    var employ = new Employee(account, name, email, password, workTime, salary, position, dayBegin);
    var flag = true;
    flag &= checkEmpty(employ.account, '#tbTKNV', 'Tài khoản') &
        checkEmpty(employ.name, '#tbTen', 'Tên') &
        checkEmpty(employ.email, '#tbEmail', 'Email') &
        checkEmpty(employ.password, '#tbMatKhau', 'Mật khẩu') &
        checkEmpty(employ.dayBegin, '#tbNgay', 'Ngày làm') &
        checkEmpty(employ.position, '#tbChucVu', 'chức vụ') &
        checkEmpty(employ.workTime, '#tbGiolam', 'Thời gian làm việc') &
        checkEmpty(employ.salary, '#tbLuongCB', 'Lương');

    flag &= checkNumberOnly(employ.account, '#tbTKNV', 'Tài khoản') &
        checkCharOnly(employ.name, '#tbTen', 'Tên') &
        checkEmail(employ.email, '#tbEmail', 'Email') &
        checkPassWord(employ.password, '#tbMatKhau', 'Mật khẩu') &
        checkTime(employ.dayBegin, '#tbNgay', 'Ngày làm') &
        checkMinMax(employ.salary, '#tbLuongCB', 'Lương cơ bản', 1000000, 20000000) &
        checkMinMax(employ.workTime, '#tbGiolam', 'Thời gian làm việc', 80, 200);

    if (!flag) {
        return;
    }
    return employ;
}

var renderForm = function(emp) {
    document.getElementById("tknv").value = emp.account;
    document.getElementById("tknv").disabled = true;
    document.getElementById("name").value = emp.name;
    document.getElementById("email").value = emp.email;
    document.getElementById("password").value = emp.password;
    document.getElementById("datepicker").value = emp.dayBegin;
    document.getElementById("chucvu").value = emp.position;
    document.getElementById("gioLam").value = emp.workTime;
    document.getElementById("luongCB").value = emp.salary;
}

document.getElementById("btnThem").addEventListener("click", function() {
    document.getElementById("tknv").disabled = false;
    document.getElementById("tknv").value = "";
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    document.getElementById("datepicker").value = "";
    document.getElementById("gioLam").value = "";
    document.getElementById("luongCB").value = "";
});