var Employee = function(account, name, email, password, workTime, salary, position, dayBegin) {
    this.account = account;
    this.name = name;
    this.email = email;
    this.password = password;
    this.workTime = workTime;
    this.salary = salary;
    this.position = position;
    this.dayBegin = dayBegin;
    this.sumSalary = function() {
        if (this.position == "Sếp") {
            return this.salary * 3;
        } else if (this.position == "Trưởng phòng") {
            return this.salary * 2;
        } else if (this.position == "Nhân viên") {
            return this.salary;
        }
    };
    this.typeEmploy = function() {
        if (this.workTime >= 192) {
            return "Xuất sắc";
        } else if (this.workTime >= 176) {
            return "Giỏi";

        } else if (this.workTime >= 160) {
            return "Khá";
        } else if (this.workTime < 160) {
            return "Trung bình";
        }


    };
};