var listEmploy = [];

// JSON
var dataList = localStorage.getItem("list");
if (dataList !== null) {
    var arrEmploy = JSON.parse(dataList);
    for (var i = 0; i < arrEmploy.length; i++) {
        var item = arrEmploy[i];
        var employ = new Employee(item.account, item.name, item.email, item.password, item.workTime, item.salary, item.position, item.dayBegin);
        listEmploy.push(employ);
    }
    rendertable(listEmploy);
}

function rendertable(listEmploy) {
    var contentHTML = "";
    for (var i = 0; i < listEmploy.length; i++) {
        var emp = listEmploy[i];
        var contentTr = `<tr>
                            <td>${emp.account}</td>
                            <td>${emp.name}</td>
                            <td>${emp.email}</td>
                            <td>${emp.dayBegin}</td>
                            <td>${emp.position}</td>
                            <td>${emp.sumSalary ()}</td>
                            <td>${emp.typeEmploy()}</td>
                            <td> 
                                <button class="btn btn-danger" onclick="deleteEmploy(${emp.account})">Xoá</button>
                                <button class="btn btn-primary"data-toggle="modal" data-target="#myModal" onclick="addForm(${emp.account})">Sửa</button>
                            </td>
                        </tr>`;
        contentHTML = contentHTML + contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function deleteEmploy(acc) {
    var viTri = findIndex(listEmploy, acc);
    listEmploy.splice(viTri, 1);
    rendertable(listEmploy);
    addLocal();
}

function updateEmploy() {
    var emp = getEmployees();
    var viTri = findIndex(listEmploy, emp.account);
    listEmploy[viTri] = emp;
    rendertable(listEmploy);
    addLocal();

}

function addForm(acc) {
    var viTri = findIndex(listEmploy, acc);
    if (viTri == -1) {
        return;
    }
    var employ = listEmploy[viTri];
    renderForm(employ);


}

function search() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("searchName");
    filter = input.value.toUpperCase();
    table = document.getElementById("tableDanhSach");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[6];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function checkAccount(listEmploy, account) {
    var viTri = findIndex(listEmploy, account);
    if (viTri == -1) {

        document.getElementById("tbTKNV").innerHTML = "";
        return true;
    } else {
        document.getElementById("tbTKNV").innerHTML = "Tài Khoản Đã Tồn Tại";
        return false;
    }
}



function addLocal() {
    var jsonList = JSON.stringify(listEmploy);
    localStorage.setItem("list", jsonList);
}

function addEmploy() {
    var Employ = getEmployees();
    var isValid = checkAccount(listEmploy, Employ.account);
    if (isValid == true) {


        listEmploy.push(Employ);
        rendertable(listEmploy);
        addLocal();
    }
}